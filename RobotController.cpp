#include "RobotController.h"
#include <iostream>

using namespace std;

RobotController::RobotController(float *leftSpeed, float *rightSpeed)
{
    this->leftSpeed = leftSpeed;
    this->rightSpeed = rightSpeed;
}

float RobotController::constraint(float value, float min, float max)
{
    if (value > max)
        value = max;

    else if (value < min)
        value = min;

    return value;
}

void RobotController::setMode(SteeringMode mode)
{
    mode = mode;
}

void RobotController::process(float currentSteering, float currentSpeed)
{
    cout << "process"
         << "\n";
    currentSteering = constraint(currentSteering, -100, 100);
    float diffSpeed = (currentSteering / 100.0);

    if (diffSpeed > 0)
    {
        *leftSpeed = currentSpeed;
        *rightSpeed = currentSpeed - (diffSpeed * currentSpeed);
    }
    else
    {
        *rightSpeed = currentSpeed;
        *leftSpeed = currentSpeed + (diffSpeed * currentSpeed);
    }
}