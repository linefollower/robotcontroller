#ifndef ROBOT_CTR_H
#define ROBOT_CTR_H

enum SteeringMode
{
    CenterPivot,
    Steering
};

class RobotController
{
private:
    // OUTPUT
    float *leftSpeed;
    float *rightSpeed;

    // MODE
    SteeringMode mode = SteeringMode::Steering;

    float constraint(float value, float min, float max);

public:
    void setMode(SteeringMode mode);
    RobotController(float *leftspeed, float *rightSpeed);

    void process(float currentSteering, float currentSpeed);
};

#endif